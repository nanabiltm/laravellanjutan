@extends('layout.master')

@section('judul')
	Halaman List Genre Film 
@endsection

@section('content')
<h1>Media Online</h1>
<h2>Sosial Media Developer</h2>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h2>Benefit Join di Media Online</h1>
<ul>
	<li> Mendapatkan motivasi dari sesama Developer</li>
	<li> Berbagi pengetahuan</li>
	<li> Dibuat oleh calon web developer terbaik</li>
</ul>
<h2>Cara Bergabung ke Media Online</h1>
<ol>
	<li> Mengunjungi Website ini</li>
	<li> Mendaftarkan di <a href="/register">Form Sign Up</a></li>
	<li> Selesai</li>
</ol>
@endsection
	
	
